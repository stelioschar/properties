# HostMaker Properties

## Contents
This assignment is based on [create-react-app](https://github.com/facebook/create-react-app)  and contains 3 main sectors
- App, which is hosted under `/src/`
    - Server mocks using [dyson.js](https://github.com/webpro/dyson) under `/server-mocks/`
- Unit Tests
- E2E tests using cypress.io under `/cypress/`

### Requirements
This app has been tested under node version `v8.11.4`.

Before proceed please check your current node version by running `node -v`.

> You can have more than one node version in the same machine by using
[nvm](https://github.com/creationix/nvm)


### Installation
Install the dependencies. You can use either `npm` or `yarn`

`npm install` or `yarn install`

### Run the app

Before launching the app you need to run the mock server by running `yarn mock`.
This will launch a test server under `:5000` port and you have access to
[http://localhost:5000/api/properties](http://localhost:5000/api/properties)

Open another terminal in parallel and run `yarn start` to launch the app server and open [http://localhost:3000/](http://localhost:3000/) in your browser

Things I could add:
- react-intl - internalization
- sass-loader - support for scss
- css-modules - isolation of css components
- eslint-loader - enforce the eslint rules in the runtime through webpack, helps you to eliminate the error and keep consistency

### Run tests

I have written some tests that include shallow render using Enzyme and some tests that show how you can mock imported modules/components.
I haven't tested all the app due to limited availability, but if I had the time I would have used Enzyme more clever by using its interactive api like `simulate()` etc.

> Snapshot testing is one of the testing ways but is more useful as an integration test.

Run `yarn test` to run the tests.


### Run E2E test

I have setup cypress as a good start in order to have an end-to-end testing in a real browser.
Cypress has a great number of features and is one of the most stable E2E testing framework.
The written tests are very basic, but in a real app what I would implement is to take screenshots and compare them with the previous ones, mock xhr requests to test edge case and more fancy things.

> App server `yarn start` (ensure that is running under `:3000`) and mock server `yarn mock` should be running in order to run cypress. Cypress support mocking xhr requests and is a great future addition for this project

Run `yarn cypress:open` to launch Cypress.

