import React from 'react';
import PropTypes from 'prop-types';

const Pficon = ({ type, className }) => (
  <span className={`icon icon-${type} ${className}`} />
);

Pficon.propTypes = {
  type: PropTypes.string,
  className: PropTypes.string,
};

Pficon.defaultProps = {
  type: '',
  className: '',
};

export default Pficon;
