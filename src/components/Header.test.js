import React from 'react';
import { shallow } from 'enzyme';
import Header from './Header';

describe('Header component', () => {
  it('should match the Header snapshot', () => {
    const renderedComponent = shallow(
      <Header />,
    );
    expect(renderedComponent).toMatchSnapshot();
  })
  it('should have alt equals to logo in the img tag', () => {
    const renderedComponent = shallow(
      <Header />,
    );
    expect(renderedComponent.find('img').prop('alt')).toEqual('logo');
  });
})
