import React from 'react';
import Properties from '../properties/Properties';
import Header from './Header';

import '../assets/fonts/icomoon/style.css';

const App = () => (
  <div>
    <Header />
    <Properties />
  </div>
);

export default App;
