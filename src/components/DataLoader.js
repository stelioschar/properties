import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import { apiRequest } from '../services/apiRequest';

export class DataLoader extends Component {
  static propTypes = {
    isDataEmpty: PropTypes.func,
    children: PropTypes.oneOfType([
      PropTypes.element,
      PropTypes.func
    ]).isRequired,
    url: PropTypes.string.isRequired,
  };

  static defaultProps = {
    isDataEmpty: data => {
      if (data === undefined) {
        return true;
      }
      return Array.isArray(data)
        ? data.length === 0
        : Object.keys(data).length === 0;
    },
  };

  state = {
    data: undefined,
    isLoading: false,
    error: undefined,
  };

  fetchData = async ({url}) => {
    try {
      const data = await apiRequest({ url });
      this.setState({
        data: data.response,
        error: null,
        isLoading: false,
      });
    } catch (error) {
      this.setState({
        error,
        isLoading: false,
      });
    }
  };

  componentDidMount() {
    return this.fetchData(this.props);
  }

  render() {
    const { data, error, isLoading, showIsLoading } = this.state;
    const { isDataEmpty } = this.props;

    if (error) {
      return (<div>Error loading data</div>);
    }

    const showNoData = !isLoading && isDataEmpty(data);

    return (
      <Fragment>
        {showIsLoading && <div>loading...</div>}
        {showNoData ? (
          <p>No data available</p>
        ) : (
          this.props.children({
            data,
          })
        )}
      </Fragment>
    );
  }
}
