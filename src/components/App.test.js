import React from 'react';
import { shallow } from 'enzyme';
import App from './App';

describe('App component', () => {
  it('should match the App snapshot', () => {
    const renderedComponent = shallow(
      <App />,
    );
    expect(renderedComponent).toMatchSnapshot();
  })
  it('should contain Header component', () => {
    const renderedComponent = shallow(
      <App />,
    );
    expect(renderedComponent.find('Header').length).toBe(1);
  })
  it('should contain properties component', () => {
    const renderedComponent = shallow(
      <App />,
    );
    expect(renderedComponent.find('Properties').length).toBe(1);
  });
})
