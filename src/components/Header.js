import React from 'react';
import './Header.css';
import logo from '../assets/svg/hostmaker-logo.svg';

const Header = () => (
  <div className="header">
    <a href="/"><img src={logo} alt="logo" /></a>
  </div>
);

export default Header;
