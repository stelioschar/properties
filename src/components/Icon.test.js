import React from 'react';
import { shallow } from 'enzyme';
import Icon from './Icon';

describe('Icon component', () => {
  it('should match the Icon snapshot', () => {
    const renderedComponent = shallow(
      <Icon />,
    );
    expect(renderedComponent).toMatchSnapshot();
  })
  it('should render `icon icon-ok` class when type is test', () => {
    const renderedComponent = shallow(
      <Icon
        type="ok"
      />,
    );
    expect(renderedComponent.find('span').hasClass('icon icon-ok')).toBe(true);
  });
})
