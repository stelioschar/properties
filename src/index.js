import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';

import 'normalize.css/normalize.css';
import './assets/main.css';

require('dotenv').config();

ReactDOM.render(
  <App />,
  document.getElementById('root'),
);
registerServiceWorker();
