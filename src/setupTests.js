import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

const googleMock = {
  maps: {
    geometry: {
      spherical: {
        computeDistanceBetween: jest.fn()
      }
    },
    LatLng: jest.fn(),
  },
};
global.google = googleMock
