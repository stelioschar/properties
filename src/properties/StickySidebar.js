import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import Sticky from 'sticky-sidebar';

class StickySidebar extends Component {
  static propTypes = {
    children: PropTypes.element.isRequired,
    stickyClass: PropTypes.string.isRequired,
    containerClass: PropTypes.string.isRequired,
    innerWrapperClass: PropTypes.string.isRequired,
  };

  componentDidMount() {
    const { stickyClass, containerClass, innerWrapperClass } = this.props;

    new Sticky(stickyClass, {
      containerSelector: containerClass,
      innerWrapperSelector: innerWrapperClass
    });
  }

  render() {
    return (
      <Fragment>
        {this.props.children}
      </Fragment>
    )
  }
}

export default StickySidebar;
