import React from 'react';
import { shallow } from 'enzyme';
import * as MapService from './MapService';
import { PropertiesTable, PropertyRow } from './PropertiesTable';

const defaultProps = {
  properties: [
    {
      id: 1,
      owner: 'Essie Morris',
      address: {
        line1: 'Room 2',
        line2: 'Tower Mansions',
        line3: 'Off Station road',
        line4: '447 Fituw Road',
        postCode: 'T3X 8N2',
        city: 'London',
        country: 'U.K.',
      },
      airbnbId: 'cce78548-f575-5863-8814-759e592e2eb1',
      numberOfBedrooms: 3,
      numberOfBathrooms: 1,
      incomeGenerated: 36426.04,
      coordinates: { latitude: 51.513338, longitude: -0.129327 },
    },
    {
      id: 5,
      owner: 'Caleb Mendez',
      address: {
        line1: 'Apartment 4',
        line3: 'Off Station road',
        line4: '630 Dijfu Avenue',
        postCode: 'V4U 4K2',
        city: 'London',
        country: 'U.K.',
      },
      airbnbId: 'efa9531f-495c-5642-931a-0cefc6f3b59f',
      numberOfBedrooms: 8,
      numberOfBathrooms: 2,
      incomeGenerated: 50676.96,
      coordinates: { latitude: 50.920728, longitude: 0.540377 },
    },
  ],
};

describe('PropertiesTable component', () => {
  it('should match the PropertiesTable snapshot', () => {
    const renderedComponent = shallow(
      <PropertiesTable
        {...defaultProps}
      />,
    );
    expect(renderedComponent).toMatchSnapshot();
  });
  it('should render two PropertyRow components', () => {
    const renderedComponent = shallow(
      <PropertiesTable
        {...defaultProps}
      />,
    );
    expect(renderedComponent.find(PropertyRow).length).toBe(2);
  });
  it('should render the second PropertyRow with id equals to 5', () => {
    const renderedComponent = shallow(
      <PropertiesTable
        {...defaultProps}
      />,
    );
    expect(renderedComponent.find(PropertyRow).at(1).props().id).toEqual(5);
  });
});

describe('PropertyRow component', () => {
  it('should match the PropertyRow snapshot', () => {
    const renderedComponent = shallow(
      <PropertyRow
        {...defaultProps.properties[0]}
      />,
    );
    expect(renderedComponent).toMatchSnapshot();
  });
  it('should render a checkmark Icon when in radius', () => {
    MapService.isInRadius = jest.fn().mockReturnValue(true);
    const renderedComponent = shallow(
      <PropertyRow
        {...defaultProps.properties[1]}
      />,
    );
    expect(renderedComponent.find('.properties__radius').props().type).toEqual('checkmark');
  });
  it('should render a cross Icon when NOT in radius', () => {
    MapService.isInRadius = jest.fn().mockReturnValue(false);
    const renderedComponent = shallow(
      <PropertyRow
        {...defaultProps.properties[1]}
      />,
    );
    expect(renderedComponent.find('.properties__radius').props().type).toEqual('cross');
  });
});
