/* global google */

import { SERVICE_AREA_MAP_CENTER } from './constants';

export const isInRadius = (coordinates) => {
  const distance = google.maps.geometry.spherical.computeDistanceBetween(
    new google.maps.LatLng({
      lat: SERVICE_AREA_MAP_CENTER.latitude,
      lng: SERVICE_AREA_MAP_CENTER.longitude,
    }),
    new google.maps.LatLng({
      lat: coordinates.latitude,
      lng: coordinates.longitude,
    }),
  );

  return distance < SERVICE_AREA_MAP_CENTER.radius;
};

export const getBounds = () => new google.maps.LatLngBounds();

export const getPoint = (lat, lng) => new google.maps.LatLng(lat, lng);
