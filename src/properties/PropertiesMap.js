import React from 'react';
import {
  Circle, GoogleMap, Marker, withGoogleMap,
} from 'react-google-maps';
import hash from 'object-hash';
import { getBounds, getPoint } from './MapService';

import { SERVICE_AREA_MAP_CENTER } from './constants';


const PropertiesMap = withGoogleMap(({ properties }) => {
  const bounds = getBounds();

  const coordinates = properties.map(({
    coordinates: {
      latitude,
      longitude,
    },
  }) => {
    const latLng = getPoint(latitude, longitude);
    bounds.extend(latLng);
    return latLng;
  });

  return (
    <GoogleMap
      ref={map => map && map.fitBounds(bounds)}
      defaultZoom={8}
      defaultCenter={{
        lat: SERVICE_AREA_MAP_CENTER.latitude,
        lng: SERVICE_AREA_MAP_CENTER.longitude,
      }}
    >
      {coordinates.map(({ lat, lng }) => (
        <Marker
          key={hash(lat())}
          position={{ lat: lat(), lng: lng() }}
        />
      ))}
      <Circle
        options={{
          strokeColor: '#58c4c6',
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: '#58c4c6',
          fillOpacity: 0.35,
        }}
        radius={20000}
        center={{
          lat: SERVICE_AREA_MAP_CENTER.latitude,
          lng: SERVICE_AREA_MAP_CENTER.longitude,
        }}
      />
    </GoogleMap>
  );
});

export default PropertiesMap;
