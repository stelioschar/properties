import React from 'react';
import PropTypes from 'prop-types';
import { isInRadius } from './MapService';
import Icon from '../components/Icon';

export const PropertyRow = ({
  id,
  owner,
  address: {
    line1,
    line2,
    line3,
    line4,
    postCode,
    city,
    country,
  },
  numberOfBedrooms,
  numberOfBathrooms,
  incomeGenerated,
  coordinates,
}) => {
  const renderAmenity = ({ value, icon, text }) => ( // eslint-disable-line
    <div>
      <Icon type={icon} />
      {' '}
      {value}
      {' '}
      {text}
    </div>
  );

  const renderAddressLine = addressLine => addressLine && (
    <div>
      {addressLine}
    </div>
  );

  return (
    <tr>
      <th>
        {id}
      </th>
      <td>
        {owner}
      </td>
      <td>
        <div>
          <b>Address:</b>
          {renderAddressLine(line1)}
          {renderAddressLine(line2)}
          {renderAddressLine(line3)}
          {renderAddressLine(line4)}
        </div>

        <div>
          <b>Postal Code</b>: {postCode || '-'}
        </div>

        <div>
          <b>City</b>: {city || '-'}
        </div>

        <div>
          <b>Country</b>: {country || '-'}
        </div>

        {isInRadius(coordinates)
          ? (
            <div>
              Inside of service area's bounds
              <Icon
                type="checkmark"
                className="properties__radius properties__radius--success"
              />
            </div>
          ) : (
            <div>
              Outside of service area's bounds
              <Icon
                type="cross"
                className="properties__radius properties__radius--alert"
              />
            </div>
          )
        }

        <hr />
        <h5>Amenities:</h5>
        {numberOfBedrooms && renderAmenity({
          value: numberOfBedrooms,
          icon: 'sleepy2',
          text: 'bedrooms',
        })}
        {numberOfBathrooms && renderAmenity({
          value: numberOfBathrooms,
          icon: 'man-woman',
          text: 'bathrooms',
        })}
      </td>
      <td>
        &#163;
        {incomeGenerated}
      </td>
    </tr>
  );
};

PropertyRow.propTypes = {
  id: PropTypes.number.isRequired,
  owner: PropTypes.string.isRequired,
  address: PropTypes.object.isRequired,
  numberOfBedrooms: PropTypes.number.isRequired,
  numberOfBathrooms: PropTypes.number.isRequired,
  incomeGenerated: PropTypes.number.isRequired,
  coordinates: PropTypes.object.isRequired,
};

export const PropertiesTable = ({ properties }) => (
  <table className="table">
    <thead>
      <tr>
        <th># id</th>
        <th>Owner</th>
        <th>Property Details</th>
        <th>Income</th>
      </tr>
    </thead>
    <tbody>
      {properties && properties.map(property => <PropertyRow key={property.id} {...property} />)}
    </tbody>
  </table>
);

PropertiesTable.propTypes = {
  properties: PropTypes.array.isRequired,
};

export default PropertiesTable;
