import React from 'react';
import PropTypes from 'prop-types';

import { DataLoader } from '../components/DataLoader';
import PropertiesMap from './PropertiesMap';
import PropertiesTable from './PropertiesTable';
import StickySidebar from './StickySidebar';

import './Properties.css';

export class PropertiesView extends React.Component {
  static propTypes = {
    properties: PropTypes.array,
  };

  static defaultProps = {
    properties: [],
  };

  render () {
    const { properties } = this.props;

    return (
      <StickySidebar
        stickyClass=".properties__map"
        containerClass=".properties__layout-col"
        innerWrapperClass=".properties__map-inner"
      >
        <div className="properties__layout">
          <div className="properties__layout-col properties__layout-col--main">
            <div className="properties__holder">
              <h3>Properties</h3>
              <PropertiesTable properties={properties} />
            </div>
          </div>
          <div className="properties__layout-col properties__layout-col--map">
            <div className="properties__map">
              <div className="properties__holder">
                <h3>Map</h3>
                <PropertiesMap
                  properties={properties}
                  containerElement={<div style={{ height: '500px' }} />}
                  mapElement={<div style={{ height: '100%' }} />}
                />
              </div>
            </div>
          </div>
        </div>
      </StickySidebar>
    )
  };
}


const Properties = ({ location }) => (
  <DataLoader
    url="/properties"
    location={location}
    >
    {({ data }) => (
      <PropertiesView properties={data} />
    )}
  </DataLoader>
);

Properties.propTypes = {
  location: PropTypes.object,
};

export default Properties;
