import React from 'react';
import { shallow } from 'enzyme';
import * as Sticky from 'sticky-sidebar';
import StickySidebar from './StickySidebar';

jest.mock('sticky-sidebar');

describe('PropertiesTable component', () => {
  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    Sticky.mockClear();
  });

  it('should match the StickySidebar snapshot', () => {
    const renderedComponent = shallow(
      <StickySidebar
        stickyClass=".stickyClass"
        containerClass=".containerClass"
        innerWrapperClass=".innerWrapperClass"
      >
        <div>awesome child</div>
      </StickySidebar>,
    );

    expect(renderedComponent).toMatchSnapshot();
  });

  it('should call the Sticky constructor with the passed props', () => {
    shallow(
      <StickySidebar
        stickyClass=".stickyClass"
        containerClass=".containerClass"
        innerWrapperClass=".innerWrapperClass"
      >
        <div>awesome child</div>
      </StickySidebar>,
    );

    expect(Sticky.default).toHaveBeenCalledWith('.stickyClass', {
      containerSelector: '.containerClass',
      innerWrapperSelector: '.innerWrapperClass',
    });
  });
});
