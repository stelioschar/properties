describe('The Home Page', () => {
  describe('render the properties list', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000') // change URL to match your dev URL
    })
    it('should take a screenshot', () => {
      cy.screenshot()
    });
  })
  describe('render GBP currency formatted prices', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000') // change URL to match your dev URL
    })
    it('should render £76067.18 for the first property', () => {
      cy.get('tbody > :nth-child(1) > :nth-child(4)')
        .should('contain', '£76067.18')
    });

    it('should render £78040.27 for the last property', () => {
      cy.get('tbody > :nth-child(5) > :nth-child(4)')
        .should('contain', '£78040.27')
    });
  })

  describe('render the property radius check', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000') // change URL to match your dev URL
    })
    it('should render the checkmark icon when inside the radius', () => {
      cy.get(':nth-child(4) > :nth-child(3) > :nth-child(5) > .properties__radius')
        .should('have.class', 'icon-checkmark')
    });
    it('should render the cross icon when outside the radius', () => {
      cy.get(':nth-child(5) > :nth-child(3) > :nth-child(5) > .properties__radius')
        .should('have.class', 'icon-cross')
    });
  })
})
