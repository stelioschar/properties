const property = require('./property');

module.exports = {
  path: '/api/properties',
  cache: true,
  collection: true,
  size: 5,
  template: property.template,
};
