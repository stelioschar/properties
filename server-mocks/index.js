const dyson = require('dyson');
const path = require('path');

dyson.bootstrap({
  configDir: path.join(__dirname),
  port: 5000,
});
