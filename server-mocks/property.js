const Chance = require('chance');
const chance = new Chance();
let propertyId = 1;
module.exports = {
  path: '/api/properties/:propertyId',
  template:  {
    id: () => propertyId,
    owner: () => chance.name(),
    address: () => {
      const optionalAdress = {};
      if (chance.pickone([true, false])) {
        optionalAdress.line2 = 'Tower Mansions';
      }
      if (chance.pickone([true, false])) {
        optionalAdress.line3 = 'Off Station road';
      }
      return {
        line1: chance.pickone(['Flat 5', 'Room 2', 'Apartment 4']),
        ...optionalAdress,
        line4: chance.address(),
        postCode: chance.postal(),
        city: 'London',
        country: 'U.K.',
      };
    },
    airbnbId: () => chance.guid(),
    numberOfBedrooms: () => chance.integer({ min: 1, max: 10 }),
    numberOfBathrooms: () => chance.integer({ min: 1, max: 3 }),
    incomeGenerated: () => {
      const incomes = [
        76067.18,
        16509.7,
        92933.71,
        33852.68,
        78040.27,
      ];

      return incomes[propertyId - 1];
    },
    coordinates: () => {
      const coords = [
        {
          latitude: 51.513338,
          longitude: -0.129327,
        },
        {
          latitude: 51.506871,
          longitude: -0.147784,
        },
        {
          latitude: 51.554477,
          longitude: -0.103848,
        },
        {
          latitude: 51.518775,
          longitude: -0.245428,
        },
        {
          latitude: 50.920728,
          longitude: 0.540377,
        },
      ];

      const currentCoord = coords[propertyId - 1];
      propertyId++;
      return currentCoord;
    },
  },
};
